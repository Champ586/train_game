﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchControl : MonoBehaviour {
	
	// Update is called once per frame
	void Update () {
		if(Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
         {
             print ("Touch Screen : " + Input.GetTouch(0).position);
             Vector3 Pos = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);
             print("Pos " + Pos);
         }
	}
}
