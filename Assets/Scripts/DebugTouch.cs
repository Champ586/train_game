﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugTouch : MonoBehaviour {

    List<string> touchInfos = new List<string>();
	
	// Update is called once per frame
	void Update () {
	   touchInfos.Clear();
	   
	   for(int i = 0; i < Input.touchCount; i++){
	   
	      Touch touch = Input.GetTouch(i);
		  string tmp = "Touch #" + (i+1) + " at " + touch.position.ToString() + ", r=" + touch.radius;
		  touchInfos.Add(tmp);
	   
	   }
		
	}
	
	void OnGui(){
	
	   foreach(string s in touchInfos)
	   {
	        GUILayout.Label(s);
	   }
	}
}
