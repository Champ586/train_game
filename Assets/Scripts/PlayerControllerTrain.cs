﻿using UnityEngine;
using System.Collections;

public class PlayerControllerTrain : MonoBehaviour {

    public GameObject[] waypoints;
	public int num = 0;

	public float speed;
	public float minDist;
	
	public bool rand=false;
	public bool go = true;

   // private Rigidbody rb;

    void Start ()
    {
        //rb = GetComponent<Rigidbody>();
    }
	
	void Update(){
	  float dist = Vector3.Distance(gameObject.transform.parent.position, waypoints[num].transform.position);
	  
	  if (go)
	  {
	       if (dist > minDist){
		       Move();
		   }
		   else {
		        if (!rand){
				    if (num+1==waypoints.Length)
					                 {
									    num=0;
									 }
					else {
					    num++;
					
					}
				}
                 else {
				 num = Random.Range(0, waypoints.Length);
				 }
		   }
	   }
	}
	
	
	public void Move(){
	
	    //gameObject.transform.parent.LookAt(waypoints[num].transform.position);
		//gameObject.transform.parent.position += gameObject.transform.parent.forward * speed * Time.deltaTime;


        var q1 = Quaternion.LookRotation(waypoints[num].transform.position - gameObject.transform.parent.position);

        gameObject.transform.parent.rotation = Quaternion.Slerp(gameObject.transform.parent.rotation, q1, Time.deltaTime);

        gameObject.transform.parent.position += ((waypoints[num].transform.position - gameObject.transform.parent.position) * Time.deltaTime * speed);

    }
	
	
/*
    void FixedUpdate ()
    {
        float moveHorizontal = Input.GetAxis ("Horizontal");
        float moveVertical = Input.GetAxis ("Vertical");

        Vector3 movement = new Vector3 (moveHorizontal, 0.0f, moveVertical);

        rb.AddForce (movement * speed);
    }
	*/
}
