﻿using UnityEngine;
using UnityEngine.Advertisements;

public class TestController : MonoBehaviour {

    public Transform rail;
    private Transform[] waypoints;
    private int num = 0;
    private int direction = 1;

    public float speed;
    public float minDist;
    public bool isFinished = false;

    void Start () {
        //waypoints = rail.transform.GetComponentsInChildren<Transform>();
        //num = waypoints.Length;
        int children = rail.childCount;
        waypoints = new Transform[children];
        for (int i = 0; i < children; ++i)
            waypoints[i] = rail.GetChild(i);
    }
	
	void Update ()
    {
        if (!isFinished)
        {
            if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                direction = 1;
            }
            if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                direction = 2;
            }
            if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                direction = 3;
            }

            float dist = Vector3.Distance(gameObject.transform.position, waypoints[num].position);
            //Debug.Log(dist);
            //Debug.Log(waypoints[num].name);
            //Debug.Log(gameObject.name);
            //Debug.Log(gameObject.transform.position);
            //Debug.Log(waypoints[num].position);
            //Debug.Break();

            if (dist > minDist)
            {
                Move();
            }
            else
            {
                if (num + 1 == waypoints.Length)
                {
                    num = 0;
                    //a = !a;
                    string tag = null;
                    //switch (direction)
                    //{
                    //    case 1:
                    //        tag = "forward";
                    //        break;
                    //    case 2:
                    //        tag = "right";
                    //        break;
                    //    case 3:
                    //        tag = "left";
                    //        break;
                    //}
                    rail = FindClosestRail(direction);
                    if (rail==null)
                    {
                        isFinished = true;
                        if (Advertisement.IsReady())
                        {
                            Advertisement.Show();
                        }
                        return;
                    }
                    int children = rail.childCount;
                    waypoints = new Transform[children];
                    for (int i = 0; i < children; ++i)
                        waypoints[i] = rail.GetChild(i);
                }
                else
                {
                    num++;

                }
            }
        }
        
    }

    public void Move()
    {
        
        var q1 = Quaternion.LookRotation(waypoints[num].position - gameObject.transform.position);

        gameObject.transform.rotation = Quaternion.Slerp(gameObject.transform.rotation, q1, Time.deltaTime + 0.1f);

        gameObject.transform.position += ((waypoints[num].position - gameObject.transform.position) * Time.deltaTime * speed);
        
        
        /*
        gameObject.transform.LookAt(waypoints[num].position);
        gameObject.transform.position += gameObject.transform.forward * speed * Time.deltaTime;
        */

    }


    Transform FindClosestRail(int direction)
    {
        GameObject[] gos;
        gos = GameObject.FindGameObjectsWithTag("cross");
        GameObject closest = null;
        GameObject closestPrev = null;
        float distance = Mathf.Infinity;
        float distancePrev = Mathf.Infinity;
        Vector3 position = transform.position;
        foreach (GameObject go in gos)
        {
            Vector3 diff = go.transform.position - position;
            float curDistance = diff.sqrMagnitude;
            if (curDistance < distance)
            {
                closestPrev = closest;
                closest = go;
                distancePrev = distance;
                distance = curDistance;
            }
        }
        if ((gameObject.transform.position - GameObject.FindGameObjectWithTag("finish").transform.position).sqrMagnitude < 50)
        {
            Debug.Log("Win!");
            return null;
        } else if (distance>50)
        {
            Debug.Log("Fail!");
            gameObject.transform.position += gameObject.transform.forward * speed * Time.deltaTime * 10;
            return null;
        }
        string name = null;
        switch (direction)
        {
            case 1:
                name = "rail_forward";
                break;
            case 2:
                name = "rail_right";
                break;
            case 3:
                name = "rail_left";
                break;
        }
        Transform res = closest.transform.Find(name);
        if (res==null)
        {
            res = closest.transform.Find("rail_forward");
        }
        if (res == null)
        {
            res = closest.transform.Find("rail_right");
        }
        if (res == null)
        {
            res = closest.transform.Find("rail_left");
        }
        return res;
        /*
        if (closestPrev != null && distancePrev < 30)
        {
            return closestPrev;
        }
        else
        {
            return closest;
        }
        */
    }

    /*
    GameObject FindClosestRail(string tag)
    {
        GameObject[] gos;
        gos = GameObject.FindGameObjectsWithTag(tag);
        GameObject closest = null;
        GameObject closestPrev = null;
        float distance = Mathf.Infinity;
        float distancePrev = Mathf.Infinity;
        Vector3 position = transform.position;
        foreach (GameObject go in gos)
        {
            Vector3 diff = go.transform.position - position;
            float curDistance = diff.sqrMagnitude;
            if (curDistance < distance)
            {
                closestPrev = closest;
                closest = go;
                distancePrev = distance;
                distance = curDistance;
            }
        }
        if (closestPrev!=null && distancePrev<30)
        {
            return closestPrev;
        } else
        {
            return closest;
        }
    }
    */
}
